<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    //
    protected $table = 'account';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'numerocuenta', 'tipocuenta', 'divisa','user_id',
    ];

    public function creditcard()
    {
        return $this->hasOne('Models\Credit','account_id');
    }
    
    public function debitcard()
    {
        return $this->hasOne('Models\Debit','account_id');
    }

    public function user()
    {
        return $this->hasOne('Models\User','id','user_id');
    }
}
