<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    //
    protected $table = 'credit';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'numerotargeta', 'limitecredito', 'deuda',
    ];

    public function account()
    {
        return $this->hasOne('Models\Account', 'id', 'account_id');
    }
}
