<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Debit extends Model
{
    //
    protected $table = 'debit';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'numerotargeta', 'cantidad',
    ];

    public function account()
    {
        return $this->hasOne('Models\Account', 'id', 'account_id');
    }
 
}
