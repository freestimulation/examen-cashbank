<?php

namespace Modules\Account\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Models\User;
use Models\Account;
use Illuminate\Support\Str;
Use Exception;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($id)
    {
        try
        {
            $account = Account::findOrFail($id);
            $dataAccount['account'] = $account;
            $dataAccount['user'] = $account->user()->first();
            $dataAccount['credit'] = $account->creditcard()->first();
            $dataAccount['debit'] = $account->debitcard()->first();
            return response()->json($dataAccount, 201);
        }
        catch (Exception $e)
        {
            return response()->json(["message" => $e->getMessage()], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request tipocuenta, divisa, user_id,
     * @return Response
     */
    public function store(Request $request)
    {
        //'tipocuenta', 'divisa','user_id',
        try
        {
            $accountData = $request->all();
            $userId = $accountData["user_id"];
            $user = User::findOrFail($userId);
            $accountData['numerocuenta'] = Str::random(5);
            $account = new Account;
            $account->fill($accountData);
            $account->save();
            return response()->json($user, 201);

        }
        catch (Exception $e)
        {
            return response()->json(["message" => $e->getMessage()], 400);  
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        try 
        {
            $accountData = $request->all();            
            $account = Account::findOrFail($id);
            $account->update($accountData);
            return response()->json($account, 200);
        } 
        catch (Exception $e)
        {
            return response()->json(["message" => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        try
        {
            $account = Account::findOrFail($id);
            $creditcard = $account->creditcard()->first();
            $debitcard = $account->debitcard()->first();

            if (isset($creditcard))
            {
                $creditcard->delete();
            }   

            if (isset($debitcard))
            {
                $debitcard->delete();
            } 

            $account->delete();
            
            return response()->json(null, 204);
        }
        catch (Exception $e)
        {
            return response()->json(["message" => $e->getMessage()], 400);
        }
    }
}
