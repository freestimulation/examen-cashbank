<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('account')->group(function() {
    Route::get('/{id}', 'AccountController@index');
    Route::post('', 'AccountController@store');
    Route::put('/{id}', 'AccountController@update');    
    Route::delete('/{id}', 'AccountController@destroy');    
});
