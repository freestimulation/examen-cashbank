<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('credit')->group(function() {
    Route::post('', 'CreditController@store');	
    Route::get('/{id}', 'CreditController@show');
    Route::delete('/{id}', 'CreditController@destroy');
    Route::put('/{id}', 'CreditController@update');
    Route::put('/{id}/withdraw', 'CreditController@withdraw');
    Route::put('/{id}/deposit', 'CreditController@deposit');   
});
