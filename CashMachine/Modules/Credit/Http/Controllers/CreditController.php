<?php

namespace Modules\Credit\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Models\Credit;
use Models\Account;
use Illuminate\Support\Str;
Use Exception;

class CreditController extends Controller
{

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try
        {
            $creditData = $request->all();
            $accountId = $creditData["account_id"];
            $account = Account::find($accountId);

            if (!isset($account))
            {
                throw new Exception('Can not create a credit without a account first');         
            }

            $creditcard = Credit::where('account_id', '=', $accountId);

            if (count($creditcard) > 0)
            {
                throw new Exception('account can not have more than one credit card');
            }

            $credit = new Credit;
            $creditData['limitecredito'] = 1000;
            $creditData['numerotargeta'] = Str::random(5);
            $creditData['deuda'] = 0;
            $credit->fill($creditData);
            $credit->save();
            return response()->json($credit, 201);

        }
        catch (Exception $e)
        {
            return response()->json(["message" => $e->getMessage()], 400);
        }    
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        try 
        {
            $credit = Credit::findOrFail($id);

            return response()->json($credit, 201);

        } 
        catch (Exception $e)
        {
            return response()->json(["message" => $e->getMessage()], 400);
        }

    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        try 
        {
            $creditData = $request->all();            
            $credit = Credit::findOrFail($id);
            $credit->update($debitData);
            return response()->json($credit, 200);
        } 
        catch (Exception $e)
        {
            return response()->json(["message" => $e->getMessage()], 400);
        }        
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $credit = Credit::findOrFail($id);
            if ($credit->deuda > 0)
            {
                throw new Exception('you must pay your credit before delete');                
            }    
            $credit->delete();
            return response()->json($credit, 204);
        }
        catch (Exception $e)
        {
            return response()->json(["message" => $e->getMessage()], 400);
        }
    }

    /*
        Por cuestiones de tiempo no se pudo poner la autentificación 
        para verificar la identidad del usuario y posteriormente se llevara acabo el 
        retiro
    */
    public function withdraw(Request $request, $id)
    {
        try
        {
            $creditData = $request->all();
            $withdraw = $creditData["withdraw"];
            $credit = Credit::findOrFail($id);

            $moneyAvalue = $credit->limitecredito - $credit->deuda;

            if ($moneyAvalue <  $withdraw)
            {
                throw new Exception('Insufficient funds');
            }

            $credit->deuda = $credit->deuda + $withdraw*(1.10);
            $credit->save();
            return response()->json($credit, 200);

        }
        catch (Exception $e)
        {
            return response()->json(["message" => $e->getMessage()], 400);
        }
    }

    public function deposit(Request $request, $id)
    {
        try
        {
            $creditData = $request->all();
            $deposit = $creditData["deposit"];
            $credit = Credit::findOrFail($id);

            $credit->deuda = $credit->deuda - $deposit;
            $credit->save();
            return response()->json($credit, 200);

        }
        catch (Exception $e)
        {
            return response()->json(["message" => $e->getMessage()], 400);
        }
    }

}
