<?php

namespace Modules\Credit\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CreditDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::table('credit')->insert([
            'account_id' => 4,
            'numerotargeta' => Str::random(5),
            'limitecredito' => 1000,
            'deuda' => 0
        ]);

        // $this->call("OthersTableSeeder");
    }
}
