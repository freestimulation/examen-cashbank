<?php

namespace Modules\Debit\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Models\Debit;
use Models\Account;
use Illuminate\Support\Str;
Use Exception;

class DebitController extends Controller
{

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try
        {
            $debitData = $request->all();
            $accountId = $debitData["account_id"];
            $account = Account::find($accountId);

            if (!isset($account))
            {
                throw new Exception('Can not create a debit without a account first');             
            }

            $debitcard = Debit::where('account_id', '=', $accountId);

            if (count($debitcard) > 0)
            {
                throw new Exception('account can not have more than one debit card');
            }

            $debit = new Debit;
            $debitData['cantidad'] = 1000;
            $debitData['numerotargeta'] = Str::random(5);    
            $debit->fill($debitData);
            $debit->save();            
            return response()->json($debit, 201);

        }
        catch (Exception $e)
        {
            return response()->json(["message" => $e->getMessage()], 400);  
        }        
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $debit = Debit::findOrFail($id);
            return response()->json($debit, 201);
        }
        catch (Exception $e)
        {
            return response()->json(["message" => $e->getMessage()], 400);
        }
    }


    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try 
        {
            $debitData = $request->all();           
            $debit = Debit::findOrFail($id);
            $debit->update($debitData);
            return response()->json($debit, 200);
        } 
        catch (Exception $e)
        {
            return response()->json(["message" => $e->getMessage()], 400);
        }        
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        //
        try
        {
            $debit = Debit::findOrFail($id);
            $debit->delete();
            return response()->json(null, 204);
        }
        catch (Exception $e)
        {
            return response()->json(["message" => $e->getMessage()], 400);
        }        
    }

    /*
        Por cuestiones de tiempo no se pudo poner la autentificación 
        para verificar la identidad del usuario y posteriormente se llevara acabo el 
        retiro
    */
    public function withdraw(Request $request, $id)
    {
        try
        {
            $debitData = $request->all();
            $withdraw = $debitData["withdraw"];
            $debit = Debit::findOrFail($id);

            if ($debit->cantidad <  $withdraw)
            {
                throw new Exception('Insufficient funds');
            }

            $debit->cantidad = $debit->cantidad -  $withdraw;
            $debit->save();
            return response()->json($debit, 200);

        }
        catch (Exception $e)
        {
            return response()->json(["message" => $e->getMessage()], 400);
        }
    }


    public function deposit(Request $request, $id)
    {
        try
        {
            $debitData = $request->all();
            $deposit = $debitData["deposit"];
            $debit = Debit::findOrFail($id);

            $debit->cantidad = $debit->cantidad + $deposit;
            $debit->save();
            return response()->json($debit, 200);

        }
        catch (Exception $e)
        {
            return response()->json(["message" => $e->getMessage()], 400);
        }
    }

}
