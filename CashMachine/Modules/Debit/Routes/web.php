<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('debit')->group(function() {
    Route::get('/{id}', 'DebitController@show');
    Route::post('', 'DebitController@store');
    Route::delete('/{id}', 'DebitController@destroy');
    Route::put('/{id}', 'DebitController@update');
    Route::put('/{id}/withdraw', 'DebitController@withdraw');
    Route::put('/{id}/deposit', 'DebitController@deposit');    
});
