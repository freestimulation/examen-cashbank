<?php

namespace Modules\Debit\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class DebitDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::table('debit')->insert([
            'account_id' => 4,
            'numerotargeta' => Str::random(5),
            'cantidad' => 1000
        ]);
        // $this->call("OthersTableSeeder");
    }
}
