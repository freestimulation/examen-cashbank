<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Models\User;
Use Exception;

class UserController extends Controller
{

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try
        {
            $userData = $request->all();
            $userData['password'] =  Hash::make($userData['password']);
            $user = new User;
            $user->fill($userData);
            $user->save();
            return response()->json($user, 201);
        }
        catch (Exception $e)
        {
            return response()->json(["message" => $e->getMessage()], 400);
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $user = User::findOrFail($id);
            return response()->json($user, 201);
        }
        catch (Exception $e)
        {
            return response()->json(["message" => $e->getMessage()], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        try
        {            
            $userData = $request->all();
            $userData['password'] =  Hash::make($userData['password']);            
            $user = User::findOrFail($id);
            $user->update($userData);
            return response()->json($user, 200);
        }
        catch (Exception $e) 
        {
            return response()->json(["message" => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        try
        {
            $user = User::findOrFail($id);
            $user->delete();
            return response()->json(null, 204);
        }
        catch (Exception $e) 
        {
            return response()->json(["message" => $e->getMessage()], 400);         
        }
    }
}
