<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

class UserDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    protected $faker = null;

    public function __construct(Faker $faker) {
        $this->faker = $faker;
    }

    public function run()
    {
        Model::unguard();
        DB::table('users')->insert([
            'nombre' => $this->faker->name,
            'apellido' => $this->faker->name,
            'email' =>  $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'rfc' =>  Str::random(5),
            'password' => Hash::make('12345')
        ]);

    }
}
