<?php

use Illuminate\Database\Seeder;
use Modules\Account\Database\Seeders\AccountDatabaseSeeder;
use Modules\Credit\Database\Seeders\CreditDatabaseSeeder;
use Modules\Debit\Database\Seeders\DebitDatabaseSeeder;
use Modules\User\Database\Seeders\UserDatabaseSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database for the tables
     * account, credit, debit, user
     *
     * @return void
     */
    public function run()
    {

	    $this->call([
	        //AccountDatabaseSeeder::class,
	        CreditDatabaseSeeder::class,
	        DebitDatabaseSeeder::class,
	        //UserDatabaseSeeder::class,	        
	    ]);

    }
}
